"""0-Ecrire un programme python qui demande les notes et nom des élèves,
calcule la moyenne et les
ordonne puis affiche les résultats.
"""
# declarer un dictionnaire
record = {}
# definir la fction qui demande et enregistre nom et notes
def lire_notes():
    # lecture sequentielle des notes et noms
    nom = input(" Entre le nom: ")
    note = (input(" Entre la note sur 20: "))
    while not(note.isdigit()):
        note = (input(" Entre la note valable sur 20: "))
    note = int(note)

    while nom != " ":
         record[nom] = note
        nom = input(" Entre le nom suivant: ")
        note = input(" Entre la note sur 20: ")
        while not (note.isdigit()):
            note = (input(" Entre la note valable sur 20: "))
        note = int(note)
        print(f'bonjour {nom} qui a {note}')
    
    return
# ranger les nom par ordre alpha et merite
def ranger():
    print(" Par ordre alphabetique")
    items = list(record.items())
    items.sort()
    for i in items:
        print(f"{i[0]} : {i[1]}")
    print(" par ordre de merite")
    items = [(i[1], i[0]) for i in items]
    items.sort()
    for i in items:
        print(f"{i[1]} : {i[0]}")
    """
    Alternativement on peut aussi ecrire
 
    items = [(i[1], i[0]) for i in items]
    for i in items:
    print(i)
    """
    return

lire_notes()
ranger()
